function xy = plotaAerodromo(caminhoDiretorio)
arquivosKML = dir([caminhoDiretorio '*.kml']);
x = [];y = [];
for i = 1: length(arquivosKML)
    nomeDiretorioComArquivo = fullfile(caminhoDiretorio, arquivosKML(i).name);
    [xi yi ] = read_kml(nomeDiretorioComArquivo);
    xy(i).latitudes = xi;
    xy(i).longitudes = yi;
    plot(xy(i).latitudes, xy(i).longitudes,'k');hold on;
end
% axis equal;
end