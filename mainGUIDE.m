function varargout = mainGUIDE(varargin)
% MAINGUIDE MATLAB code for mainGUIDE.fig
%      MAINGUIDE, by itself, creates a new MAINGUIDE or raises the existing
%      singleton*.
%
%      H = MAINGUIDE returns the handle to a new MAINGUIDE or the handle to
%      the existing singleton*.
%
%      MAINGUIDE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAINGUIDE.M with the given input arguments.
%
%      MAINGUIDE('Property','Value',...) creates a new MAINGUIDE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mainGUIDE_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mainGUIDE_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mainGUIDE

% Last Modified by GUIDE v2.5 23-May-2017 15:17:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @mainGUIDE_OpeningFcn, ...
                   'gui_OutputFcn',  @mainGUIDE_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mainGUIDE is made visible.
function mainGUIDE_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mainGUIDE (see VARARGIN)

% Choose default command line output for mainGUIDE
handles.output = hObject;
[xy, handles] = plotaAerodromo('C:\Guilherme\1.Programas\campanhaXHALE\ensaioSoloXHALE\2.arquivos\', handles, hObject);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes mainGUIDE wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = mainGUIDE_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function [xy, handles] = plotaAerodromo(caminhoDiretorio, handles, hObject)
arquivosKML = dir([caminhoDiretorio '*.kml']);
x = [];y = [];
for i = 1: length(arquivosKML)
    nomeDiretorioComArquivo = fullfile(caminhoDiretorio, arquivosKML(i).name);
    [xi yi ] = read_kml(nomeDiretorioComArquivo);
    xy(i).latitudes = xi;
    xy(i).longitudes = yi;
    axes(handles.axes2);hold on;
    grid minor;
    figura01 = plot(xy(i).latitudes, xy(i).longitudes,'k');hold on;
end
handles.eixoOriginal = axis;
handles.plotAerodromo = figura01;
% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
figura01 = handles.axes2;
retangulo = getrect(figura01);
x = [retangulo(1) retangulo(1)+retangulo(3)]';
y = [retangulo(2) retangulo(2)+retangulo(4)]';
axis([x(1) x(2) y(1) y(2)])
guidata(hObject, handles);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Update handles structure
handles.axes2;
axis(handles.eixoOriginal);
axis equal;
guidata(hObject, handles);


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.endereco = 'C:\Guilherme\1.Programas\campanhaXHALE\ensaioSoloXHALE\LiveRecorderFile.log';
plotaSerieDados(handles);
guidata(hObject, handles);

function dadosGravados = plotaSerieDados(handles)
endereco =handles.endereco;
dadosGravados=[];
TOP=0;
tempoExecucao = 0;
set(handles.pushbutton4, 'UserData', 0);
set(handles.pushbutton6, 'UserData', 0);
stopper = get(handles.pushbutton4,'UserData');
% cla;
AC=[];
% refereciaLongitudeGrau = -referenciaGrauLongitude - referenciaMinLongitude/60 - referenciaSegLongitude/3600;
% axes(handles.axes1);
% plot(refereciaLongitudeGrau, refereciaLatitudeGrau,'md','MarkerFaceColor','b','MarkerSize',10);hold on;
% refereciaAltura = get(handles.edit8,'String'); refereciaAltura = str2num(refereciaAltura);
% axes(handles.axes2);
% plot(0.5, refereciaAltura,'mo','MarkerFaceColor','b','MarkerSize',10);hold on;
% handles.circ95 = [];
% handles.circ100 = [];
% handles.normal = [];
% raioCircunferencia = [];
% deltaAlturaGPSColetadas = [];
while  stopper == 0
    delete(AC);
    dado = leitorLogFile(endereco);
    marker = get(handles.pushbutton6,'UserData');
    if marker == 1
        TOP = TOP+1;
        set(handles.text27,'String',num2str(TOP),'FontSize',12,'HorizontalAlignment',...
         'center', 'SelectionHighLight','on','FontWeight','bold');
        set(handles.pushbutton6, 'UserData', 0);
    end
    dadosGravados = [dadosGravados; [TOP dado]];
    hold on;axes(handles.axes2);
    AC = plot(-45.86, -23.22,'m*');hold on;
    AC = plot(dado(33), dado(32),'m*');hold on; 
    set(handles.text1,'String',dado(14),'FontSize',12,'HorizontalAlignment',...
         'center', 'SelectionHighLight','on','FontWeight','bold');
    hAx = axes('Position',[0.375 0.67 0.01 0.20]);
    [hAx,hPatch] = thermometer(hAx,[0 5 20],dado(14));
    set(hAx,'Color','r');           
    set(hPatch,'FaceColor','b');
    set(handles.text2,'String',dado(9),'FontSize',12,'HorizontalAlignment',...
         'center', 'SelectionHighLight','on','FontWeight','bold');
    hAx = axes('Position',[0.505 0.67 0.01 0.20]);
    [hAx,hPatch] = thermometer(hAx,[0 1 5],dado(9));
    set(gca,'yaxisLocation','right');
    set(hAx,'Color','m');           
    set(hPatch,'FaceColor',[.7 .5 0]);
    set(handles.text3,'String',dado(36),'FontSize',12,'HorizontalAlignment',...
         'center', 'SelectionHighLight','on','FontWeight','bold');
    axes(handles.axes5);
    plotarGauge(dado(36)); 
    drawnow;
     
%      raio100 = max(raioCircunferencia);
%      set(handles.text34,'String',raio100*1852*60,'FontSize',12,'HorizontalAlignment',...
%          'center', 'SelectionHighLight','on','FontWeight','bold');
%      set(handles.text37,'String',dado(39),'FontSize',12,'HorizontalAlignment',...
%                  'center', 'SelectionHighLight','on','FontWeight','bold');
%     drawnow;
%     determinaTempo(handles,dado(38), handles.text7, handles.text8, handles.text9);
%     determinaTempo(handles,dado(1)-dadosGravados(1,1), handles.text10, handles.text11, handles.text12);
%     determinaTempo(handles,dado(38)-dadosGravados(1,38), handles.text20, handles.text21, handles.text22);
%     tempoAquisicaoGPS = (dado(38) - dadosGravados(1,38))*.001;
%     tempoAquisicaoCLOCK = (dado(1) - dadosGravados(1,1))*.001;
%     set(handles.text29,'String',tempoAquisicaoGPS,'FontSize',12,'HorizontalAlignment',...
%                  'center', 'SelectionHighLight','on','FontWeight','bold');
%     set(handles.text30,'String',tempoAquisicaoCLOCK,'FontSize',12,'HorizontalAlignment',...
%      'center', 'SelectionHighLight','on','FontWeight','bold');
    stopper = get(handles.pushbutton4,'UserData');
end
save('PARAM.mat','dadosGravados');

function plotarGauge(valor)
plot([valor valor], [0 -1.5], '-g', 'Linewidth', 2);
axis([ valor-30 valor+30 -1 0]);
set(gca,'YTick',[], 'xaxisLocation','top','Color','white');

function dados = leitorLogFile(endereco)
file = fopen(endereco);
dados = textscan(file,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ');
dados = cell2mat(dados);
dados = dados(end, :);
fclose(file);


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton4, 'UserData', 1);
guidata(hObject,handles);


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton6, 'UserData', 1);
guidata(hObject,handles);
